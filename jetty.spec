%global jtuid       110
%global username    %{name}
%global confdir     %{_sysconfdir}/%{name}
%global logdir      %{_localstatedir}/log/%{name}
%global homedir     %{_datadir}/%{name}
%global jettycachedir %{_localstatedir}/cache/%{name}
%global tempdir     %{jettycachedir}/temp
%global rundir      %{_localstatedir}/run/%{name}
%global jettylibdir %{_localstatedir}/lib/%{name}
%global appdir      %{jettylibdir}/webapps
%global addver  .v20190411
%bcond_with     jp_minimal
Name:                jetty
Version:             9.4.16
Release:             9
Summary:             Java Webserver and Servlet Container
License:             Apache-2.0 OR EPL-1.0
URL:                 http://www.eclipse.org/jetty/
Source0:             https://github.com/eclipse/%{name}.project/archive/%{name}-%{version}%{addver}.tar.gz
Source1:             jetty.sh
Source3:             jetty.logrotate
Source5:             %{name}.service
Source6:             LICENSE-MIT
Patch0:              CVE-2020-27216.patch
Patch1:              CVE-2020-27223.patch
Patch2:              CVE-2021-28165.patch
Patch3:              CVE-2021-28169.patch
Patch4:              CVE-2021-34428.patch
Patch5:              CVE-2022-2047.patch
Patch6:              CVE-2022-2048.patch
Patch7:              CVE-2023-26048.patch
Patch8:              CVE-2023-26049.patch
Patch9:              CVE-2023-36479.patch
Patch10:             CVE-2023-40167.patch
Patch11:             CVE-2024-6762.patch

Patch1000:			 0001-fix-race-condition-that-may-make-the-bucket-cleaning.patch

BuildRequires:       maven-local mvn(javax.servlet:javax.servlet-api) < 4.0.0
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-shade-plugin)
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin) mvn(org.slf4j:slf4j-api)
%if %{without jp_minimal}
BuildRequires:       maven-local mvn(com.github.jnr:jnr-unixsocket)
BuildRequires:       mvn(javax.annotation:javax.annotation-api) mvn(javax.enterprise:cdi-api)
BuildRequires:       mvn(javax.servlet:javax.servlet-api) < 4.0.0
BuildRequires:       mvn(javax.servlet.jsp:javax.servlet.jsp-api) mvn(javax.servlet:jstl)
BuildRequires:       mvn(javax.transaction:javax.transaction-api)
BuildRequires:       mvn(javax.websocket:javax.websocket-api)
BuildRequires:       mvn(javax.websocket:javax.websocket-client-api) mvn(org.apache.ant:ant)
BuildRequires:       mvn(org.apache.ant:ant-launcher) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven:maven-artifact) mvn(org.apache.maven:maven-core)
BuildRequires:       mvn(org.apache.maven:maven-plugin-api) mvn(org.apache.maven:maven-project)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-failsafe-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-plugin-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-remote-resources-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-shade-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-war-plugin)
BuildRequires:       mvn(org.apache.maven.plugin-tools:maven-plugin-annotations)
BuildRequires:       mvn(org.apache.maven.plugin-tools:maven-plugin-tools-api)
BuildRequires:       mvn(org.apache.maven.shared:maven-artifact-transfer)
BuildRequires:       mvn(org.apache.taglibs:taglibs-standard-impl)
BuildRequires:       mvn(org.apache.taglibs:taglibs-standard-spec)
BuildRequires:       mvn(org.apache.tomcat:tomcat-jasper)
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:       mvn(org.codehaus.mojo:exec-maven-plugin) mvn(org.eclipse.equinox.http:servlet)
BuildRequires:       mvn(org.eclipse.jetty.alpn:alpn-api)
BuildRequires:       mvn(org.eclipse.jetty.orbit:javax.mail.glassfish)
BuildRequires:       mvn(org.eclipse.jetty.orbit:javax.security.auth.message)
BuildRequires:       mvn(org.eclipse.jetty.toolchain:jetty-assembly-descriptors)
BuildRequires:       mvn(org.eclipse.jetty.toolchain:jetty-schemas)
BuildRequires:       mvn(org.eclipse.jetty.toolchain:jetty-test-helper)
BuildRequires:       mvn(org.eclipse.osgi:org.eclipse.osgi)
BuildRequires:       mvn(org.eclipse.osgi:org.eclipse.osgi.services)
BuildRequires:       mvn(org.infinispan:infinispan-core)
BuildRequires:       mvn(org.jboss.weld.servlet:weld-servlet-core)
BuildRequires:       mvn(org.mongodb:mongo-java-driver) mvn(org.ow2.asm:asm)
BuildRequires:       mvn(org.ow2.asm:asm-commons) mvn(org.slf4j:slf4j-api)
BuildRequires:       mvn(org.springframework:spring-beans)
BuildRequires:       java-1.8.0-openjdk-headless java-1.8.0-openjdk
BuildRequires:       mvn(org.eclipse.jetty.toolchain:jetty-artifact-remote-resources)
BuildRequires:       mvn(org.eclipse.jetty.toolchain:jetty-distribution-remote-resources)
BuildRequires:       mvn(org.eclipse.jetty.toolchain:jetty-test-policy) maven-javadoc-plugin
BuildRequires:       glassfish-el systemd junit5
BuildRequires:       jboss-websocket-1.0-api
Requires:            jboss-websocket-1.0-api
Requires:            java-1.8.0-openjdk-headless
Requires:            glassfish-servlet-api < 4.0.0
%endif # without jp_minimal
BuildArch:           noarch
%if %{without jp_minimal}
Requires:            javapackages-tools %{name}-annotations = %{version}-%{release}
Requires:            %{name}-ant = %{version}-%{release} %{name}-client = %{version}-%{release}
Requires:            %{name}-continuation = %{version}-%{release}
Requires:            %{name}-deploy = %{version}-%{release}
Requires:            %{name}-fcgi-client = %{version}-%{release}
Requires:            %{name}-fcgi-server = %{version}-%{release}
Requires:            %{name}-http = %{version}-%{release} %{name}-http-spi = %{version}-%{release}
Requires:            %{name}-io = %{version}-%{release} %{name}-jaas = %{version}-%{release}
Requires:            %{name}-jaspi = %{version}-%{release} %{name}-jmx = %{version}-%{release}
Requires:            %{name}-jndi = %{version}-%{release} %{name}-jsp = %{version}-%{release}
Requires:            %{name}-jspc-maven-plugin = %{version}-%{release}
Requires:            %{name}-maven-plugin = %{version}-%{release}
Requires:            %{name}-plus = %{version}-%{release} %{name}-proxy = %{version}-%{release}
Requires:            %{name}-rewrite = %{version}-%{release}
Requires:            %{name}-security = %{version}-%{release} %{name}-server = %{version}-%{release}
Requires:            %{name}-servlet = %{version}-%{release}
Requires:            %{name}-servlets = %{version}-%{release} %{name}-spring = %{version}-%{release}
Requires:            %{name}-start = %{version}-%{release}
Requires:            %{name}-unixsocket = %{version}-%{release} %{name}-util = %{version}-%{release}
Requires:            %{name}-util-ajax = %{version}-%{release}
Requires:            %{name}-webapp = %{version}-%{release} %{name}-xml = %{version}-%{release}
Requires:            %{name}-infinispan = %{version}-%{release} %{name}-cdi = %{version}-%{release}
Requires:            %{name}-websocket-api = %{version}-%{release}
Requires:            %{name}-websocket-client = %{version}-%{release}
Requires:            %{name}-websocket-common = %{version}-%{release}
Requires:            %{name}-websocket-server = %{version}-%{release}
Requires:            %{name}-websocket-servlet = %{version}-%{release}
Requires:            %{name}-javax-websocket-client-impl = %{version}-%{release}
Requires:            %{name}-javax-websocket-server-impl = %{version}-%{release}
Requires:            %{name}-nosql = %{version}-%{release}
Requires:            %{name}-httpservice = %{version}-%{release}
Requires:            %{name}-osgi-boot = %{version}-%{release}
Requires:            %{name}-osgi-boot-warurl = %{version}-%{release}
Requires:            %{name}-osgi-boot-jsp = %{version}-%{release}
Requires:            %{name}-osgi-alpn = %{version}-%{release}
Requires:            %{name}-quickstart = %{version}-%{release} %{name}-jstl = %{version}-%{release}
Requires:            %{name}-alpn-client = %{version}-%{release}
Requires:            %{name}-alpn-server = %{version}-%{release}
Requires:            %{name}-http2-client = %{version}-%{release}
Requires:            %{name}-http2-common = %{version}-%{release}
Requires:            %{name}-http2-hpack = %{version}-%{release}
Requires:            %{name}-http2-http-client-transport = %{version}-%{release}
Requires:            %{name}-http2-server = %{version}-%{release}
Requires(pre):    shadow-utils
%{?systemd_ordering}
Provides:            group(%username) = %jtuid
Provides:            user(%username) = %jtuid
%endif # without jp_minimal
Obsoletes:           %{name}-manual < 9.4.0-0.4
Obsoletes:           %{name}-ajp < 9.4.0-0.4
Obsoletes:           %{name}-nested < 9.4.0-0.4
Obsoletes:           %{name}-overlay-deployer < 9.4.0-0.4
Obsoletes:           %{name}-policy < 9.4.0-0.4
Obsoletes:           %{name}-websocket-mux-extension < 9.4.0-0.4
Obsoletes:           %{name}-runner < 9.4.0-0.4
Obsoletes:           %{name}-osgi-npn < 9.4.0-0.4
Obsoletes:           %{name}-monitor < 9.4.0-0.4
Obsoletes:           %{name}-hazelcast < 9.4.14-1
%description
%global desc \
Jetty is a 100% Java HTTP Server and Servlet Container. This means that you\
do not need to configure and run a separate web server (like Apache) in order\
to use Java, servlets and JSPs to generate dynamic content. Jetty is a fully\
featured web server for static and dynamic content. Unlike separate\
server/container solutions, this means that your web server and web\
application run in the same process, without interconnection overheads\
and complications. Furthermore, as a pure java component, Jetty can be simply\
included in your application for demonstration, distribution or deployment.\
Jetty is available on all Java supported platforms.
%{desc}
%global extdesc %{desc}\
\
This package contains

%package        client
Summary:             client module for Jetty
%description    client
%{extdesc} %{summary}.

%package        continuation
Summary:             continuation module for Jetty
%description    continuation
%{extdesc} %{summary}.

%package        http
Summary:             http module for Jetty
%description    http
%{extdesc} %{summary}.

%package        http-spi
Summary:             http-spi module for Jetty
%description    http-spi
%{extdesc} %{summary}.

%package        io
Summary:             io module for Jetty
Obsoletes:           %{name}-websocket < 9.4.0-0.4
%description    io
%{extdesc} %{summary}.

%package        jaas
Summary:             jaas module for Jetty
%description    jaas
%{extdesc} %{summary}.

%package        jsp
Summary:             jsp module for Jetty
Requires:            glassfish-el
Requires:            glassfish-servlet-api < 4.0.0
%description    jsp
%{extdesc} %{summary}.

%package        security
Summary:             security module for Jetty
%description    security
%{extdesc} %{summary}.

%package        server
Summary:             server module for Jetty
Requires:            glassfish-servlet-api < 4.0.0
%description    server
%{extdesc} %{summary}.

%package        servlet
Summary:             servlet module for Jetty
%description    servlet
%{extdesc} %{summary}.

%package        util
Summary:             util module for Jetty
License:             (Apache-2.0 OR EPL-1.0) AND MIT
%description    util
%{extdesc} %{summary}.

%package        webapp
Summary:             webapp module for Jetty
%description    webapp
%{extdesc} %{summary}.

%package        jmx
Summary:             jmx module for Jetty
%description    jmx
%{extdesc} %{summary}.

%package        xml
Summary:             xml module for Jetty
%description    xml
%{extdesc} %{summary}.
%if %{without jp_minimal}

%package        project
Summary:             POM files for Jetty
Obsoletes:           %{name}-websocket-parent < 9.4.0-0.4
Provides:            %{name}-websocket-parent = %{version}-%{release}
Obsoletes:           %{name}-osgi-project < 9.4.0-0.4
Provides:            %{name}-osgi-project = %{version}-%{release}
%description    project
%{extdesc} %{summary}.

%package        deploy
Summary:             deploy module for Jetty
%description    deploy
%{extdesc} %{summary}.

%package        annotations
Summary:             annotations module for Jetty
%description    annotations
%{extdesc} %{summary}.

%package        ant
Summary:             ant module for Jetty
%description    ant
%{extdesc} %{summary}.

%package cdi
Summary:             Jetty CDI Configuration
%description cdi
%{extdesc} %{summary}.

%package        fcgi-client
Summary:             FastCGI client module for Jetty
%description    fcgi-client
%{extdesc} %{summary}.

%package        fcgi-server
Summary:             FastCGI client module for Jetty
Requires:            glassfish-servlet-api < 4.0.0
%description    fcgi-server
%{extdesc} %{summary}.

%package        infinispan
Summary:             infinispan module for Jetty
%description    infinispan
%{extdesc} %{summary}.

%package        jaspi
Summary:             jaspi module for Jetty
%description    jaspi
%{extdesc} %{summary}.

%package        jndi
Summary:             jndi module for Jetty
%description    jndi
%{extdesc} %{summary}.

%package        jspc-maven-plugin
Summary:             jspc-maven-plugin module for Jetty
%description    jspc-maven-plugin
%{extdesc} %{summary}.

%package        maven-plugin
Summary:             maven-plugin module for Jetty
%description    maven-plugin
%{extdesc} %{summary}.

%package        plus
Summary:             plus module for Jetty
%description    plus
%{extdesc} %{summary}.

%package        proxy
Summary:             proxy module for Jetty
%description    proxy
%{extdesc} %{summary}.

%package        rewrite
Summary:             rewrite module for Jetty
Requires:            glassfish-servlet-api < 4.0.0
%description    rewrite
%{extdesc} %{summary}.

%package        servlets
Summary:             servlets module for Jetty
%description    servlets
%{extdesc} %{summary}.

%package        spring
Summary:             spring module for Jetty
%description    spring
%{extdesc} %{summary}.

%package        start
Summary:             start module for Jetty
%description    start
%{extdesc} %{summary}.

%package        unixsocket
Summary:             unixsocket module for Jetty
%description    unixsocket
%{extdesc} %{summary}.

%package        util-ajax
Summary:             util-ajax module for Jetty
%description    util-ajax
%{extdesc} %{summary}.

%package        websocket-api
Summary:             websocket-api module for Jetty
%description    websocket-api
%{extdesc} %{summary}.

%package        websocket-client
Summary:             websocket-client module for Jetty
%description    websocket-client
%{extdesc} %{summary}.

%package        websocket-common
Summary:             websocket-common module for Jetty
%description    websocket-common
%{extdesc} %{summary}.

%package        websocket-server
Summary:             websocket-server module for Jetty
%description    websocket-server
%{extdesc} %{summary}.

%package        websocket-servlet
Summary:             websocket-servlet module for Jetty
Requires:            glassfish-servlet-api < 4.0.0
%description    websocket-servlet
%{extdesc} %{summary}.

%package        javax-websocket-client-impl
Summary:             javax-websocket-client-impl module for Jetty
%description    javax-websocket-client-impl
%{extdesc} %{summary}.

%package        javax-websocket-server-impl
Summary:             javax-websocket-server-impl module for Jetty
%description    javax-websocket-server-impl
%{extdesc} %{summary}.

%package        nosql
Summary:             nosql module for Jetty
%description    nosql
%{extdesc} %{summary}.

%package        httpservice
Summary:             httpservice module for Jetty
Requires:            glassfish-servlet-api < 4.0.0
%description    httpservice
%{extdesc} %{summary}.

%package        osgi-boot
Summary:             osgi-boot module for Jetty
%description    osgi-boot
%{extdesc} %{summary}.

%package        osgi-boot-warurl
Summary:             osgi-boot-warurl module for Jetty
%description    osgi-boot-warurl
%{extdesc} %{summary}.

%package        osgi-boot-jsp
Summary:             osgi-boot-jsp module for Jetty
Requires:            glassfish-servlet-api < 4.0.0
%description    osgi-boot-jsp
%{extdesc} %{summary}.

%package        osgi-alpn
Summary:             osgi-alpn module for Jetty
%description    osgi-alpn
%{extdesc} %{summary}.

%package        quickstart
Summary:             quickstart module for Jetty
%description    quickstart
%{extdesc} %{summary}.

%package        alpn-client
Summary:             alpn-client module for Jetty
%description    alpn-client
%{extdesc} %{summary}.

%package        alpn-server
Summary:             alpn-server module for Jetty
%description    alpn-server
%{extdesc} %{summary}.

%package        http2-client
Summary:             http2-client module for Jetty
%description    http2-client
%{extdesc} %{summary}.

%package        http2-common
Summary:             http2-common module for Jetty
%description    http2-common
%{extdesc} %{summary}.

%package        http2-hpack
Summary:             http2-hpack module for Jetty
%description    http2-hpack
%{extdesc} %{summary}.

%package        http2-http-client-transport
Summary:             http2-http-client-transport module for Jetty
%description    http2-http-client-transport
%{extdesc} %{summary}.

%package        http2-server
Summary:             http2-server module for Jetty
%description    http2-server
%{extdesc} %{summary}.

%package        jstl
Summary:             jstl module for Jetty
%description    jstl
%{extdesc} %{summary}.
%endif # without jp_minimal

%package javadoc
Summary:             Javadoc for %{name}
License:             (Apache-2.0 OR EPL-1.0) AND MIT
%description javadoc
%{summary}.

%prep
%autosetup -n %{name}.project-%{name}-%{version}%{addver} -p1
find . -name "*.?ar" -exec rm {} \;
find . -name "*.class" -exec rm {} \;
%pom_remove_plugin -r :findbugs-maven-plugin
%pom_remove_plugin -r :maven-enforcer-plugin
%pom_remove_plugin -r :clirr-maven-plugin
%pom_remove_plugin -r :maven-eclipse-plugin
%pom_remove_plugin -r :maven-pmd-plugin
%pom_remove_plugin -r :license-maven-plugin
%pom_remove_plugin -r :maven-site-plugin
%pom_remove_plugin -r :maven-source-plugin
%pom_remove_plugin -r :maven-deploy-plugin
%pom_remove_plugin -r :jacoco-maven-plugin
%pom_remove_plugin -r :maven-release-plugin
%pom_remove_plugin -r :buildnumber-maven-plugin
%pom_remove_plugin -r :h2spec-maven-plugin
%pom_remove_plugin -r :flatten-maven-plugin jetty-bom
%pom_remove_plugin -r :maven-dependency-plugin jetty-http2/http2-http-client-transport
%pom_remove_plugin -r :maven-dependency-plugin jetty-http2/http2-alpn-tests
%pom_remove_plugin -r :maven-dependency-plugin tests/test-http-client-transport
%pom_remove_plugin -r :maven-dependency-plugin tests/test-webapps/test-http2-webapp
%pom_disable_module aggregates/jetty-all
%pom_xpath_replace "pom:groupId[text()='ant']" "<groupId>org.apache.ant</groupId>" jetty-ant/pom.xml
%pom_remove_dep "com.sun.net.httpserver:http" jetty-http-spi
%pom_change_dep -r org.mortbay.jasper:apache-jsp org.apache.tomcat:tomcat-jasper
%pom_add_dep 'org.junit.jupiter:junit-jupiter-engine:${junit.version}' tests/test-sessions/test-sessions-common
%pom_change_dep -r javax.servlet.jsp:jsp-api javax.servlet.jsp:javax.servlet.jsp-api
%pom_remove_plugin ":jetty-version-maven-plugin"
%pom_xpath_remove "pom:artifactItem[pom:classifier='version']" jetty-home
%pom_xpath_remove 'pom:execution[pom:id="sources"]' jetty-home
sed -i '/^\s*\*.*<script>/d' jetty-util/src/main/java/org/eclipse/jetty/util/resource/Resource.java
%pom_remove_plugin :maven-invoker-plugin jetty-jspc-maven-plugin
%pom_disable_module test-jetty-osgi jetty-osgi/pom.xml
%pom_disable_module jetty-documentation
%pom_remove_dep -r :jetty-documentation
%pom_xpath_remove 'pom:execution[pom:id="unpack-documentation"]' jetty-distribution
%pom_xpath_remove 'pom:artifactItem[pom:artifactId="libsetuid-osx"]' jetty-home/pom.xml
%pom_xpath_remove "pom:execution[pom:id[text()='copy-setuid-deps']]" jetty-home/pom.xml
%pom_disable_module jetty-gcloud
%pom_disable_module test-gcloud-sessions tests/test-sessions
%pom_remove_dep :jetty-gcloud-session-manager jetty-home
%pom_disable_module jetty-memcached
%pom_disable_module test-memcached-sessions tests/test-sessions
%pom_remove_dep :jetty-memcached-sessions jetty-home
%pom_disable_module jetty-hazelcast
%pom_disable_module test-hazelcast-sessions tests/test-sessions
%pom_remove_dep :jetty-hazelcast jetty-home
%pom_disable_module jetty-jmh
%pom_disable_module test-distribution tests
%pom_disable_module jetty-alpn-conscrypt-server jetty-alpn
%pom_disable_module jetty-alpn-conscrypt-client jetty-alpn
%pom_remove_dep -r :jetty-alpn-conscrypt-server
%pom_remove_dep -r :jetty-alpn-conscrypt-client
sed -i 's|9.3.0.RC0|${project.version}|g' tests/test-webapps/test-servlet-spec/test-spec-webapp/pom.xml
rm -fr examples/embedded/src/main/java/org/eclipse/jetty/embedded/ManyConnectors.java
cp %{SOURCE6} .
sed -i '/<SystemProperty name="jetty.state"/d' \
    jetty-home/src/main/resources/etc/jetty-started.xml
%if %{with jp_minimal}
%pom_remove_plugin :maven-remote-resources-plugin
%pom_remove_plugin :maven-assembly-plugin
%pom_remove_plugin :maven-dependency-plugin jetty-client
%pom_disable_module jetty-ant
%pom_disable_module jetty-http2
%pom_disable_module jetty-fcgi
%pom_disable_module jetty-websocket
%pom_disable_module jetty-servlets
%pom_disable_module jetty-util-ajax
%pom_disable_module apache-jsp
%pom_disable_module apache-jstl
%pom_disable_module jetty-maven-plugin
%pom_disable_module jetty-jspc-maven-plugin
%pom_disable_module jetty-deploy
%pom_disable_module jetty-start
%pom_disable_module jetty-plus
%pom_disable_module jetty-annotations
%pom_disable_module jetty-jndi
%pom_disable_module jetty-cdi
%pom_disable_module jetty-spring
%pom_disable_module jetty-proxy
%pom_disable_module jetty-jaspi
%pom_disable_module jetty-rewrite
%pom_disable_module jetty-nosql
%pom_disable_module jetty-infinispan
%pom_disable_module jetty-unixsocket
%pom_disable_module tests
%pom_disable_module examples
%pom_disable_module jetty-quickstart
%pom_disable_module jetty-distribution
%pom_disable_module jetty-runner
%pom_disable_module jetty-http-spi
%pom_disable_module jetty-osgi
%pom_disable_module jetty-alpn
%pom_disable_module jetty-home
%endif # with jp_minimal

%build
%mvn_package :jetty-home __noinstall
%mvn_package :jetty-distribution __noinstall
%if %{without jp_minimal}
%mvn_package ':*-project' project
%mvn_package ':*-parent' project
%mvn_package ':*-bom' project
%else
%mvn_package ':*-project' __noinstall
%mvn_package ':*-parent' __noinstall
%mvn_package ':*-bom' __noinstall
%endif
%mvn_package :test-mock-resources
%mvn_package ':test-*' __noinstall
%mvn_package ':*-tests' __noinstall
%mvn_package ':*-it' __noinstall
%mvn_package ':example-*' __noinstall
%mvn_package org.eclipse.jetty.tests: __noinstall
%mvn_package ::war: __noinstall
%mvn_package :jetty-runner __noinstall
%mvn_package org.eclipse.jetty.cdi: jetty-cdi
%mvn_package ':jetty-alpn*-client' jetty-alpn-client
%mvn_package ':jetty-alpn*-server' jetty-alpn-server
%mvn_package :apache-jsp jetty-jsp
%mvn_alias :apache-jsp :jetty-jsp
%mvn_build -f -s

%install
%mvn_install
%if %{without jp_minimal}
cp -pr jetty-distribution/target/distribution %{buildroot}%{homedir}
install -dm 755 %{buildroot}%{_bindir}
install -dm 755 %{buildroot}%{_sysconfdir}/logrotate.d
install -dm 755 %{buildroot}%{confdir}
install -dm 755 %{buildroot}%{homedir}/start.d
install -dm 755 %{buildroot}%{logdir}
install -dm 755 %{buildroot}%{rundir}
install -dm 755 %{buildroot}%{tempdir}
install -dm 755 %{buildroot}%{jettylibdir}
install -dm 755 %{buildroot}%{_unitdir}
cp %{SOURCE5} %{buildroot}%{_unitdir}/
install -pm 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
echo '# Placeholder configuration file.  No default is provided.' > \
     %{buildroot}%{confdir}/jetty.conf
build-jar-repository %{buildroot}%{homedir}/lib/apache-jsp \
           tomcat/jasper tomcat/tomcat-juli \
           tomcat/tomcat-jsp-2.3-api tomcat/tomcat-api tomcat/tomcat-util \
           tomcat-taglibs-standard/taglibs-standard-compat \
           tomcat-taglibs-standard/taglibs-standard-impl \
           tomcat/tomcat-util-scan glassfish-el-api glassfish-el
ecj=`echo %{buildroot}%{homedir}/lib/apache-jsp/org.eclipse.jdt.ecj-*.jar`
rm $ecj
xmvn-subst -s -L -R %{buildroot} %{buildroot}%{homedir}/lib
ln -sf %{_javadir}/ecj.jar $ecj
( cat << EO_RC
JAVA_HOME=/usr/lib/jvm/java
JAVA_OPTIONS=
JETTY_HOME=%{homedir}
JETTY_CONSOLE=%{logdir}/jetty-console.log
JETTY_PORT=8080
JETTY_RUN=%{_localstatedir}/run/%{name}
JETTY_PID=\$JETTY_RUN/jetty.pid
EO_RC
) > %{buildroot}%{homedir}/.jettyrc
mkdir -p %{buildroot}%{_tmpfilesdir}
( cat << EOF
D %{rundir} 0755 %username %{username} -
EOF
) > %{buildroot}%{_tmpfilesdir}/%{name}.conf
rm -r %{buildroot}%{homedir}/logs
ln -s %{logdir} %{buildroot}%{homedir}/logs
mv %{buildroot}%{homedir}/etc/* %{buildroot}/%{confdir}/
rm -r %{buildroot}%{homedir}/etc
ln -s %{confdir} %{buildroot}%{homedir}/etc
mv %{buildroot}%{homedir}/webapps %{buildroot}%{appdir}
ln -s %{appdir} %{buildroot}%{homedir}/webapps
rm %{buildroot}%{homedir}/*.html
ln -sf %{rundir} %{buildroot}%{homedir}/work
cp -p %{SOURCE1} %{buildroot}%{homedir}/bin/jetty.sh

%pre
getent group %username >/dev/null || groupadd -f -g %jtuid -r %username
if ! getent passwd %username >/dev/null ; then
    if ! getent passwd %jtuid >/dev/null ; then
      useradd -r -u %jtuid -g %username -d %homedir -s /sbin/nologin \
      -c "Jetty web server" %username
    else
      useradd -r -g %username -d %homedir -s /sbin/nologin \
      -c "Jetty web server" %username
    fi
fi
exit 0

%post
%systemd_post jetty.service

%preun
%systemd_preun jetty.service

%postun
%systemd_postun_with_restart jetty.service
%endif # without jp_minimal

%files client -f .mfiles-jetty-client

%files continuation -f .mfiles-jetty-continuation

%files jaas -f .mfiles-jetty-jaas

%files io -f .mfiles-jetty-io

%files server -f .mfiles-jetty-server

%files servlet -f .mfiles-jetty-servlet

%files util -f .mfiles-jetty-util
%license LICENSE NOTICE.txt LICENSE-MIT

%files webapp -f .mfiles-jetty-webapp

%files jmx -f .mfiles-jetty-jmx

%files xml -f .mfiles-jetty-xml

%files http -f .mfiles-jetty-http

%files security -f .mfiles-jetty-security
%if %{without jp_minimal}

%files -f .mfiles
%{_tmpfilesdir}/%{name}.conf
%config(noreplace) %attr(644, root, root) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{confdir}
%dir %{jettylibdir}
%dir %{jettycachedir}
%{homedir}
%attr(744, jetty, jetty) %{homedir}/bin/jetty.sh
%attr(755, jetty, jetty) %{logdir}
%attr(755, jetty, jetty) %{tempdir}
%ghost %dir %attr(755, jetty, jetty) %{rundir}
%{appdir}
%{_unitdir}/%{name}.service

%files project -f .mfiles-project
%doc README.md VERSION.txt
%license LICENSE NOTICE.txt LICENSE-MIT

%files annotations -f .mfiles-jetty-annotations

%files ant -f .mfiles-jetty-ant

%files cdi -f .mfiles-jetty-cdi

%files deploy -f .mfiles-jetty-deploy

%files fcgi-client -f .mfiles-fcgi-client

%files fcgi-server -f .mfiles-fcgi-server

%files http-spi -f .mfiles-jetty-http-spi

%files infinispan -f .mfiles-jetty-infinispan

%files jaspi -f .mfiles-jetty-jaspi

%files jndi -f .mfiles-jetty-jndi

%files jsp -f .mfiles-jetty-jsp

%files jstl -f .mfiles-apache-jstl

%files jspc-maven-plugin -f .mfiles-jetty-jspc-maven-plugin

%files maven-plugin -f .mfiles-jetty-maven-plugin

%files plus -f .mfiles-jetty-plus

%files proxy -f .mfiles-jetty-proxy

%files quickstart -f .mfiles-jetty-quickstart

%files rewrite -f .mfiles-jetty-rewrite

%files servlets -f .mfiles-jetty-servlets

%files start -f .mfiles-jetty-start

%files unixsocket -f .mfiles-jetty-unixsocket

%files util-ajax -f .mfiles-jetty-util-ajax

%files websocket-api -f .mfiles-websocket-api

%files websocket-client -f .mfiles-websocket-client

%files websocket-common -f .mfiles-websocket-common

%files websocket-server -f .mfiles-websocket-server

%files websocket-servlet -f .mfiles-websocket-servlet

%files javax-websocket-client-impl -f .mfiles-javax-websocket-client-impl

%files javax-websocket-server-impl -f .mfiles-javax-websocket-server-impl

%files alpn-client -f .mfiles-jetty-alpn-client

%files alpn-server -f .mfiles-jetty-alpn-server

%files http2-client -f .mfiles-http2-client

%files http2-common -f .mfiles-http2-common

%files http2-hpack -f .mfiles-http2-hpack

%files http2-http-client-transport -f .mfiles-http2-http-client-transport

%files http2-server -f .mfiles-http2-server

%files nosql -f .mfiles-jetty-nosql

%files httpservice -f .mfiles-jetty-httpservice

%files osgi-alpn -f .mfiles-jetty-osgi-alpn

%files osgi-boot -f .mfiles-jetty-osgi-boot

%files osgi-boot-warurl -f .mfiles-jetty-osgi-boot-warurl

%files osgi-boot-jsp -f .mfiles-jetty-osgi-boot-jsp

%files spring -f .mfiles-jetty-spring
%endif # without jp_minimal

%files javadoc -f .mfiles-javadoc
%license LICENSE NOTICE.txt LICENSE-MIT

%changelog
* Tue Nov 19 2024 zhaosaisai <zhaosaisai@kylinos.cn> - 9.4.16-9
- fix race condition that may make the bucket cleaning pick the 
wrong one in case the timestamp is read while being modified

* Tue Nov 05 2024 yaoxin <yao_xin001@hoperun.com> - 9.4.16-8
- Fix CVE-2024-6762

* Tue Oct 15 2024 wangkai <13474090681@163.com> - 9.4.16-7
- Fix CVE-2023-26048,CVE-2023-26049,CVE-2023-36479,CVE-2023-40167

* Thu Jul 18 2024 yaoxin <yao_xin001@hoperun.com> - 9.4.16-6
- License compliance rectification

* Mon Jan 29 2024 xu_ping <707078654@qq.com> - 9.4.16-5
- Fix build failure due to upgrade jetty-artifact-remote-resources

* Tue Mar 14 2023 caodongxia <caodongxia@h-partners.com> - 9.4.16-4
- Fix self-compilation failure

* Wed Mar 01 2023 Ge Wang <wangge20@h-partners.com> - 9.4.16-3
- Fix EBS build failure due to jetty-util version mismatch

* Wed Dec 28 2022 jiangpeng <jiangpeng01@ncti-gba.cn> - 9.4.16-2
- Fix CVE-2022-2047 and CVE-2022-2048

* Tue Sep 13 2022 liangqifeng <liangqifeng@ncti-gba.cn> - 9.4.16-1
- update to 9.4.14 to Fix CVE-2019-10241

* Thu Jul 1 2021 wutao <wutao61@huawei.com> - 9.4.15-9
- Fix CVE-2021-34428

* Wed Jun 23 2021 wangyue <wangyue92@huawei.com> - 9.4.15-8
- Fix CVE-2021-28169

* Wed Apr 21 2021 wangxiao <wangxiao65@huawei.com> - 9.4.15-7
- Fix CVE-2021-28165

* Wed Mar 17 2021 zhanghua <zhanghua40@huawei.com> - 9.4.15-6
- fix CVE-2020-27223

* Mon Feb 8 2021 zhanghua <zhanghua40@huawei.com> - 9.4.15-5
- fix CVE-2020-27216

* Tue Nov 10 2020 wangyue <wangyue92@huawei.com> - 9.4.15-4
- Change dependence: replace jetty-alpn with openjdk-1.8.0

* Fri Sep 12 2020 chengzihan2 <chengzihan2@huawei.com> - 9.4.15-3
- disable jp_minimal

* Fri Sep 4 2020 huanghaitao <huanghaitao8@huawei.com> - 9.4.15-2
- endable jpmini

* Wed Aug 26 2020 huanghaitao <huanghaitao8@huawei.com> - 9.4.15-1
- package init
